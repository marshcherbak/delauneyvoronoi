﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulation
{
    class Triangle
    {
        public int v1, v2, v3;
        public Vertex center;
        public double radius;
        public Triangle()
        {
            v1 = -1;
            v2 = -1;
            v3 = -1;
        }
        public Triangle(int vertex1,int vertex2, int vertex3)
        {
            double x1, x2, x3, y1, y2, y3;
            double x, y;

            v1 = vertex1;
            v2 = vertex2;
            v3 = vertex3;

            x1 = PointsAndTriangles.allPoints[vertex1].x;
            x2 = PointsAndTriangles.allPoints[vertex2].x;
            x3 = PointsAndTriangles.allPoints[vertex3].x;
            y1 = PointsAndTriangles.allPoints[vertex1].y;
            y2 = PointsAndTriangles.allPoints[vertex2].y;
            y3 = PointsAndTriangles.allPoints[vertex3].y;

            //центр трикутника 
            x = ((y2 - y1) * (y3 * y3 - y1 * y1 + x3 * x3 - x1 * x1) - (y3 - y1) * (y2 * y2 - y1 * y1 + x2 * x2 - x1 * x1)) / (2 * (x3 - x1) * (y2 - y1) - 2 * ((x2 - x1) * (y3 - y1)));
            y = ((x2 - x1) * (x3 * x3 - x1 * x1 + y3 * y3 - y1 * y1) - (x3 - x1) * (x2 * x2 - x1 * x1 + y2 * y2 - y1 * y1)) / (2 * (y3 - y1) * (x2 - x1) - 2 * ((y2 - y1) * (x3 - x1)));

            center = new Vertex(x, y);
            radius = Math.Sqrt((x1 - x) * (x1 - x) + (y1 - y) * (y1 - y));


        }
        public bool isInCircumcircle(Vertex vertex)
        {
            double d_squared = (vertex.x - this.center.x) * (vertex.x - this.center.x) + (vertex.y - this.center.y) * (vertex.y - this.center.y);
            double radius_squared = this.radius * this.radius;
            return d_squared < radius_squared;
        }
        public bool SharesVertexWith(Triangle triangle)
        {
            if (this.v1 == triangle.v1) return true;
            if (this.v1 == triangle.v2) return true;
            if (this.v1 == triangle.v3) return true;

            if (this.v2 == triangle.v1) return true;
            if (this.v2 == triangle.v2) return true;
            if (this.v2 == triangle.v3) return true;

            if (this.v3 == triangle.v1) return true;
            if (this.v3 == triangle.v2) return true;
            if (this.v3 == triangle.v2) return true;

            return false;
        }
        public DelaunayEdge FindCommonEdgeWith(Triangle triangle)
        {
            DelaunayEdge commonEdge;
            List<int> commonVerticles = new List<int>();
            if (v1 == triangle.v1 || v1 == triangle.v2 || v1 == triangle.v3)
                commonVerticles.Add(v1);
            if (v2 == triangle.v1 || v2 == triangle.v2 || v2 == triangle.v3)
                commonVerticles.Add(v2);
            if (v3 == triangle.v1 || v3 == triangle.v2 || v3 == triangle.v3)
                commonVerticles.Add(v3);

            if(commonVerticles.Count == 2)
            {
                commonEdge = new DelaunayEdge(commonVerticles[0], commonVerticles[1]);
                return commonEdge;
            }

            return null;
        }
        public override bool Equals(object obj)
        {
            return this == (Triangle)obj;
        }
        public static bool operator ==(Triangle a, Triangle b)
        {
            if (((object)a) == ((object)b))
            {
                return true;
            }

            if ((((object)a) == null) || (((object)b) == null))
            {
                return false;
            }

            return ((a.v1 == b.v1 && a.v2 == b.v2 && a.v3 == b.v3) ||
                     (a.v1 == b.v1 && a.v2 == b.v3 && a.v3 == b.v2) ||
                     (a.v1 == b.v2 && a.v2 == b.v1 && a.v3 == b.v3) ||
                     (a.v1 == b.v2 && a.v2 == b.v3 && a.v3 == b.v1) ||
                     (a.v1 == b.v3 && a.v2 == b.v1 && a.v3 == b.v2) ||
                     (a.v1 == b.v3 && a.v2 == b.v2 && a.v3 == b.v1));
        }
        public static bool operator !=(Triangle a, Triangle b)
        {
            return !(a == b);
        }
        public override int GetHashCode()
        {
            return this.v1.GetHashCode() ^ this.v2.GetHashCode() ^ this.v3.GetHashCode();
        }
    }
}
