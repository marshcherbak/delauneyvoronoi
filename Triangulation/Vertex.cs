﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulation
{
    class Vertex
    {
        public double x, y, z;
        public List<int> adjoinTriangles;
        public Vertex()
        {
            this.x = 0;
            this.y = 0;
            this.z = 0;
            adjoinTriangles = new List<int>();
        }
        public Vertex(double x, double y)
        {
            this.x = x;
            this.y = y;
            this.z = 0;
            adjoinTriangles = new List<int>();
        }
        public Vertex(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            adjoinTriangles = new List<int>();
        }
        public Vertex(Vertex v)
        {
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
            adjoinTriangles = new List<int>();
        }
        public static Vertex operator +(Vertex a, Vertex b)
        {
            Vertex result = new Vertex(a.x + b.x, a.y + b.y, a.z + b.z);
            return result;
        }
        public static Vertex operator -(Vertex a, Vertex b)
        {
            Vertex result = new Vertex(a.x - b.x, a.y - b.y, a.z - b.z);
            return result;
        }
        public static Vertex operator *(double s, Vertex a)
        {
            Vertex result = new Vertex(s * a.x, s * a.y, s * a.z);
            return result;
        }
        public static Vertex operator *(Vertex a, double s)
        {
            return s * a;
        }
        public double dot(Vertex a, Vertex b)
        {
            return (a.x * b.x + a.y * b.y + a.z * b.z);
        }
        public Vertex cross(Vertex a, Vertex b)
        {
            Vertex result = new Vertex(a.y * b.z - a.z * b.y, a.z * b.x - a.x * b.z, a.x * b.y - a.y * b.x);
            return result;
        }
        public override bool Equals(object obj)
        {
            return this == (Vertex)obj;
        }
        public static bool operator ==(Vertex a, Vertex b)
        {
            if (((object)a) == ((object)b))
            {
                return true;
            }

            if ((((object)a) == null) || (((object)b) == null))
            {
                return false;
            }

            if (a.x != b.x) return false;
            if (a.y != b.y) return false;

            return true;
        }
        public static bool operator !=(Vertex a, Vertex b)
        {
            return !(a == b);
        }
        public override int GetHashCode()
        {
            int xHc = this.x.ToString().GetHashCode();
            int yHc = this.y.ToString().GetHashCode();
            int zHc = this.z.ToString().GetHashCode();

            return (xHc * 1) ^ (yHc * 2) ^ (zHc * 3);
        }
    }
}
