﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulation
{
    class Delaunay
    {
        public static List<DelaunayEdge> DelaunayEdges(Triangle superTriangle, List<Triangle> delaunayTriangles)
        {
            List<DelaunayEdge> delaunayEdges = new List<DelaunayEdge>();

            for(int i = 0; i < delaunayTriangles.Count; i++)
            {
                DelaunayEdge edge1 = new DelaunayEdge(delaunayTriangles[i].v1, delaunayTriangles[i].v2);
                DelaunayEdge edge2 = new DelaunayEdge(delaunayTriangles[i].v2, delaunayTriangles[i].v3);
                DelaunayEdge edge3 = new DelaunayEdge(delaunayTriangles[i].v3, delaunayTriangles[i].v1);

                if (!edge1.ContainsVertex(superTriangle.v1) && !edge1.ContainsVertex(superTriangle.v2) && !edge1.ContainsVertex(superTriangle.v3))
                    delaunayEdges.Add(edge1);
                if (!edge2.ContainsVertex(superTriangle.v1) && !edge2.ContainsVertex(superTriangle.v2) && !edge2.ContainsVertex(superTriangle.v3))
                    delaunayEdges.Add(edge2);
                if (!edge3.ContainsVertex(superTriangle.v1) && !edge3.ContainsVertex(superTriangle.v2) && !edge3.ContainsVertex(superTriangle.v3))
                    delaunayEdges.Add(edge3);
            }
            return delaunayEdges;
        }
        public static List<Triangle> Triangulate(Triangle superTriangle, List<Vertex> triangulationPoints)
        {
            List<Triangle> delaunayTriangles = new List<Triangle>();

            PointsAndTriangles.allTriangles = new List<Triangle>();

            List<int> open = new List<int>();
            List<int> closed = new List<int>();

            PointsAndTriangles.allTriangles.Add(superTriangle); ;
            open.Add(0);

            for (int i = 0; i < triangulationPoints.Count - 3; i++)
            {

                List<DelaunayEdge> polygon = new List<DelaunayEdge>();

                for (int j = open.Count - 1; j >= 0; j--)
                {
                    double dx = triangulationPoints.ElementAt(i).x - PointsAndTriangles.allTriangles[open[j]].center.x;

                    if (dx > 0.0 && dx * dx > PointsAndTriangles.allTriangles[open[j]].radius * PointsAndTriangles.allTriangles[open[j]].radius)
                    {
                        closed.Add(open[j]);
                        open.RemoveAt(j);
                        continue;
                    }

                    if (PointsAndTriangles.allTriangles[open[j]].isInCircumcircle(triangulationPoints.ElementAt(i)))
                    {
                        polygon.Add(new DelaunayEdge(PointsAndTriangles.allTriangles[open[j]].v1, PointsAndTriangles.allTriangles[open[j]].v2));
                        polygon.Add(new DelaunayEdge(PointsAndTriangles.allTriangles[open[j]].v2, PointsAndTriangles.allTriangles[open[j]].v3));
                        polygon.Add(new DelaunayEdge(PointsAndTriangles.allTriangles[open[j]].v3, PointsAndTriangles.allTriangles[open[j]].v1));

                        triangulationPoints[PointsAndTriangles.allTriangles[open[j]].v1].adjoinTriangles.Remove(open[j]);
                        triangulationPoints[PointsAndTriangles.allTriangles[open[j]].v2].adjoinTriangles.Remove(open[j]);
                        triangulationPoints[PointsAndTriangles.allTriangles[open[j]].v3].adjoinTriangles.Remove(open[j]);

                        open.RemoveAt(j);

                    }
                }

                for (int j = polygon.Count - 2; j >= 0; j--)
                {
                    for (int k = polygon.Count - 1; k >= j + 1; k--)
                    {
                        if (polygon[j] == polygon[k])
                        {
                            polygon.RemoveAt(k);
                            polygon.RemoveAt(j);
                            k--;
                            continue;
                        }
                    }
                }

                for (int j = 0; j < polygon.Count; j++)
                {
                    Triangle triangle = new Triangle(polygon[j].start, polygon[j].end, i);

                    int currentTriangle = PointsAndTriangles.allTriangles.Count;

                    PointsAndTriangles.allTriangles.Add(triangle);
                    open.Add(currentTriangle);

                    if (!triangulationPoints[i].adjoinTriangles.Contains(currentTriangle))
                    {
                        triangulationPoints[i].adjoinTriangles.Add(currentTriangle);
                    }
                    if (!triangulationPoints[polygon[j].start].adjoinTriangles.Contains(currentTriangle))
                    {
                        triangulationPoints[polygon[j].start].adjoinTriangles.Add(currentTriangle);
                    }
                    if (!triangulationPoints[polygon[j].end].adjoinTriangles.Contains(currentTriangle))
                    {
                        triangulationPoints[polygon[j].end].adjoinTriangles.Add(currentTriangle);
                    }

                }
            }


            for (int i = 0; i < open.Count; i++)
            {
                delaunayTriangles.Add(PointsAndTriangles.allTriangles[open[i]]);
            }

            for (int i = 0; i < closed.Count; i++)
            {
                delaunayTriangles.Add(PointsAndTriangles.allTriangles[closed[i]]);
            }

            return delaunayTriangles;
        }
        public static Triangle SuperTriangle(List<Vertex> triangulationPoints)
        {

            double M = triangulationPoints[0].x;

            for (int i = 1; i < triangulationPoints.Count; i++)
            {
                double xAbs = Math.Abs(triangulationPoints[i].x);
                double yAbs = Math.Abs(triangulationPoints[i].y);
                if (xAbs > M) M = xAbs;
                if (yAbs > M) M = yAbs;
            }

            Vertex sp1 = new Vertex(100 * M, 0, 0);
            triangulationPoints.Add(sp1);
            
            Vertex sp2 = new Vertex(0, 100 * M, 0);
            triangulationPoints.Add(sp2);
            
            Vertex sp3 = new Vertex(-100 * M, -100 * M, 0);
            triangulationPoints.Add(sp3);
            return new Triangle(triangulationPoints.Count - 3, triangulationPoints.Count - 2, triangulationPoints.Count - 1);
        }
    }
}
