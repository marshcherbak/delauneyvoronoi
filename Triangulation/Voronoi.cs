﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Triangulation
{
    class Voronoi
    {
        public static List<VoronoiEdge> VoronoiEdges(List<DelaunayEdge> delaunayEdges)
        {
            List<VoronoiEdge> voronoiEdgeList = new List<VoronoiEdge>();

            for (int i = 0; i < delaunayEdges.Count; i++)
            {
                List<int> neighbours = new List<int>();
                for (int j = 0; j < PointsAndTriangles.allPoints[delaunayEdges[i].start].adjoinTriangles.Count; j++)
                {
                    for (int k = 0; k < PointsAndTriangles.allPoints[delaunayEdges[i].end].adjoinTriangles.Count; k++)
                    {
                        if (PointsAndTriangles.allPoints[delaunayEdges[i].start].adjoinTriangles[j] == PointsAndTriangles.allPoints[delaunayEdges[i].end].adjoinTriangles[k])
                        {
                            neighbours.Add(PointsAndTriangles.allPoints[delaunayEdges[i].start].adjoinTriangles[j]);
                        }
                    }
                }
                VoronoiEdge voronoiEdge = new VoronoiEdge(PointsAndTriangles.allTriangles[neighbours[0]].center, PointsAndTriangles.allTriangles[neighbours[1]].center);
                voronoiEdgeList.Add(voronoiEdge);
            }

            return voronoiEdgeList;
        }
    }
}
