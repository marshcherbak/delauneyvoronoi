﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Triangulation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Random seeder;
        int pointCount;
        Triangle superTriangle = new Triangle();
        List<Triangle> delaunayTriangleList = new List<Triangle>();
        List<DelaunayEdge> delaunayEdgeList = new List<DelaunayEdge>();
        List<VoronoiEdge> voronoiEdgeList = new List<VoronoiEdge>();
        List<Vertex> ogPoints = new List<Vertex>();

        bool pointsSpreaded = false;
        bool delaunayGenerated = false;
        bool voronoiGenerated = false;
        public MainWindow()
        {
            InitializeComponent();
            seeder = new Random();    
        }
        public void addPoints()
        {
            pointCount = ogPoints.Count;
            for(int i = 0; i < pointCount; i++)
            {
                PointsAndTriangles.allPoints.Add(ogPoints[i]);
            }
            PointsAndTriangles.allPoints = PointsAndTriangles.allPoints.OrderBy(vertex => vertex.x).ThenBy(vertex => vertex.y).ToList();
            for (int i = 0; i < PointsAndTriangles.allPoints.Count; i++)
            {
                paintVertex(PointsAndTriangles.allPoints[i].x, PointsAndTriangles.allPoints[i].y, Brushes.Black);

            }
            pointsSpreaded = true;
        }
        public void SpreadPointsRandomly()
        {           
            PointsAndTriangles.allPoints = new List<Vertex>();
            int seed = seeder.Next();
            Random rand = new Random(seed);
            int k;
            Int32.TryParse(howManyPoints.Text, out k);
            pointCount = k;
            if (pointCount != 0)
            {
                for (int i = 0; i < pointCount; i++)
                {
                    Point p = new Point((float)(rand.NextDouble() * 976), (float)(rand.NextDouble() * 550));
                    Vertex v = new Vertex(p.X, p.Y, 0);
                    ogPoints.Add(v);
                }
                addPoints();
                pointsSpreaded = true;
            }
            else
                MessageBox.Show("No points to generate. Please enter an integer number.", "ERROR");
        }
        private void myCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Point p = new Point();
            p = e.GetPosition(myCanvas);
            Vertex vv = null;
            if (ogPoints != null)
            {
                vv = checkVertex(p);
            }

            if (vv == null)
            {
                ClearAll();
                PointsAndTriangles.allPoints = new List<Vertex>();
                ogPoints.Add(new Vertex(p.X, p.Y));
                pointCount = ogPoints.Count;
                addPoints();
                if (DelaunayButton.IsChecked == true && pointsSpreaded && !delaunayGenerated)
                {
                    DelaunayTriangulate();
                }
                if (VoronoiButton.IsChecked == true /*&& delaunayGenerated*/ && !voronoiGenerated)
                {
                    VoronoiDiagram();
                }
            }

        }
        private void paintVertex(double x, double y, SolidColorBrush color)
        {
            Ellipse point = new Ellipse();
            point.Width = 4;
            point.Height = 4;
            point.Fill = color;
            Canvas.SetLeft(point, x - 2);
            Canvas.SetTop(point, y - 2);
            myCanvas.Children.Add(point);
            
        }
        public void DelaunayTriangulate()
        {
            ClearAll();
            addPoints();
            if(PointsAndTriangles.allTriangles!=null)
            PointsAndTriangles.allTriangles.Clear();

            superTriangle = Delaunay.SuperTriangle(PointsAndTriangles.allPoints);
            delaunayTriangleList = Delaunay.Triangulate(superTriangle, PointsAndTriangles.allPoints);
            delaunayEdgeList = Delaunay.DelaunayEdges(superTriangle, delaunayTriangleList);
            for (int i = 0; i < delaunayEdgeList.Count; i++)
            {
                Point p1 = new Point((int)PointsAndTriangles.allPoints[delaunayEdgeList[i].start].x, (int)PointsAndTriangles.allPoints[delaunayEdgeList[i].start].y);
                Point p2 = new Point((int)PointsAndTriangles.allPoints[delaunayEdgeList[i].end].x, (int)PointsAndTriangles.allPoints[delaunayEdgeList[i].end].y);
                paintEdge(p1.X, p1.Y, p2.X, p2.Y, Brushes.Black);
            }
            for (int i = 0; i < PointsAndTriangles.allPoints.Count; i++)
            {
                paintVertex(PointsAndTriangles.allPoints[i].x, PointsAndTriangles.allPoints[i].y, Brushes.Black);
            }
            delaunayGenerated = true;       
        }
        private void paintEdge(double X1, double Y1, double X2, double Y2, SolidColorBrush color)
        {
            Line line = new Line();
            line.X1 = X1;
            line.X2 = X2;
            line.Y1 = Y1;
            line.Y2 = Y2;
            line.Stroke = color;
            line.StrokeThickness = 1.5;
            myCanvas.Children.Add(line);
        }
        public void VoronoiDiagram()
        {
            ClearAll();
            addPoints();
            if (PointsAndTriangles.allTriangles != null)
                PointsAndTriangles.allTriangles.Clear();
            for (int i = 0; i < PointsAndTriangles.allPoints.Count; i++)
            {
                paintVertex(PointsAndTriangles.allPoints[i].x, PointsAndTriangles.allPoints[i].y, Brushes.Black);
            }
            superTriangle = Delaunay.SuperTriangle(PointsAndTriangles.allPoints);
            delaunayTriangleList = Delaunay.Triangulate(superTriangle, PointsAndTriangles.allPoints);
            delaunayEdgeList = Delaunay.DelaunayEdges(superTriangle, delaunayTriangleList);
            for (int i = 0; i < PointsAndTriangles.allPoints.Count; i++)
            {
                paintVertex(PointsAndTriangles.allPoints[i].x, PointsAndTriangles.allPoints[i].y, Brushes.Black);
            }
            voronoiEdgeList = Voronoi.VoronoiEdges(delaunayEdgeList);
            delaunayGenerated = true;
            for (int i = 0; i < voronoiEdgeList.Count; i++)
            {
                Point p1 = new Point((int)voronoiEdgeList[i].start.x, (int)voronoiEdgeList[i].start.y);
                Point p2 = new Point((int)voronoiEdgeList[i].end.x, (int)voronoiEdgeList[i].end.y);

                paintEdge(p1.X, p1.Y, p2.X, p2.Y, Brushes.DarkOrange);
                paintVertex(p1.X, p1.Y, Brushes.DarkOrange);
                paintVertex(p2.X, p2.Y, Brushes.DarkOrange);
            }
            delaunayGenerated = false;
            voronoiGenerated = true;
        }
        private void ClearAll()
        {
            myCanvas.Children.Clear();
            if (pointsSpreaded && PointsAndTriangles.allPoints!=null)
            {
                PointsAndTriangles.allPoints.Clear();
                pointsSpreaded = false;
            }
            if (voronoiGenerated)
            {
                delaunayTriangleList.Clear();
                delaunayEdgeList.Clear();
                delaunayGenerated = false;
                PointsAndTriangles.allTriangles.Clear();
                foreach (var vvv in ogPoints)
                {
                    vvv.adjoinTriangles.Clear();
                }
                voronoiEdgeList.Clear();
                voronoiGenerated = false;
            }
            else if (delaunayGenerated)
            {
                delaunayTriangleList.Clear();
                delaunayEdgeList.Clear();
                delaunayGenerated = false;
                PointsAndTriangles.allTriangles.Clear();
                foreach (var vvv in ogPoints)
                {
                    vvv.adjoinTriangles.Clear();
                }
            }
            pointCount = 0;
            
        }
        private void GenButton_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
            ogPoints.Clear();
            SpreadPointsRandomly();
        }
        private void LoadFFButton_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "TXT Files(*.txt)| *.txt";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                // Open document 
                string filename = dlg.FileName;
                string[] graphInfo = File.ReadAllLines(dlg.FileName);
                PointsAndTriangles.allPoints = new List<Vertex>();
                int vert = 0;
                Int32.TryParse(graphInfo[0], out vert);
                for (int i = 1; i < vert + 1; ++i)
                {
                    string[] words = graphInfo[i].Split(',');

                    double coord1;
                    double coord2;
                    Double.TryParse(words[0], out coord1);
                    Double.TryParse(words[1], out coord2);
                    Vertex v = new Vertex(coord1, coord2);
                    PointsAndTriangles.allPoints.Add(v);
                    ogPoints.Add(v);
                }
                PointsAndTriangles.allPoints = PointsAndTriangles.allPoints.OrderBy(vertex => vertex.x).ToList();

                for (int i = 0; i < PointsAndTriangles.allPoints.Count; i++)
                {
                    paintVertex(PointsAndTriangles.allPoints[i].x, PointsAndTriangles.allPoints[i].y, Brushes.Black);

                }

                pointsSpreaded = true;

            }
        }
        private void SaveFFButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.Filter = "TXT (*.txt)|*.txt";
            dlg.DefaultExt = "txt";
            dlg.AddExtension = true;

            Nullable<bool> result = dlg.ShowDialog();
         if (result == true)
            {
                string sOut = "";
                int vn = ogPoints.Count;
           
                sOut += vn + "\n";
                foreach (var v in ogPoints)
                {
                    sOut += v.x + "," + v.y + "\n";
                }
                
                File.WriteAllText(dlg.FileName, sOut);
            }
        }
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            if (DelaunayButton.IsChecked == true && pointsSpreaded && !delaunayGenerated)
            {
                DelaunayTriangulate();
            }
            if (VoronoiButton.IsChecked == true && !voronoiGenerated)
            {
                VoronoiDiagram();
            }
        }
        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ClearAll();
            ogPoints.Clear();
        }
        private void howManyPoints_GotFocus(object sender, RoutedEventArgs e)
        {
            howManyPoints.Clear();
        }
        private Vertex checkVertex(Point p)
        {
            double x = p.X;
            double y = p.Y;
            if (ogPoints == null)
                return null;
            foreach (var v in ogPoints)
            {
                if (((x - v.x) * (x - v.x) + (y - v.y) * (y - v.y)) < 9)
                    return v;
            }
            return null;
        }
    }
}
